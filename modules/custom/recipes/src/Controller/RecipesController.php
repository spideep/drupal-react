<?php

namespace Drupal\recipes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RecipesController.
 */
class RecipesController extends ControllerBase
{

    /**
     * Getdata.
     *
     * @return string
     *   Return Hello string.
     */
    public function getData(Node $node)
    {
        // Get a node storage object.
        $node_storage = \Drupal::entityManager()->getStorage('node');
        // Load a single node.
        $node_storage->load($node);
        return new JsonResponse($node_storage);
    }

}
