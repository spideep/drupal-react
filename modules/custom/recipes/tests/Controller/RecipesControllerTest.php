<?php

namespace Drupal\recipes\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the recipes module.
 */
class RecipesControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "recipes RecipesController's controller functionality",
      'description' => 'Test Unit for module recipes and controller RecipesController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests recipes functionality.
   */
  public function testRecipesController() {
    // Check that the basic functions of module recipes.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
