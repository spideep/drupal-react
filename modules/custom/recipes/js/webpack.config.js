module.exports = {
  entry: ['whatwg-fetch', './AllRecipes.js'],
  output: {
    path: __dirname,
    filename: 'allrecipes.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          query: {
            presets: ['react']
          }
        }
      },
      {
        test: /\.css$/,
        use: {
          loader: 'style-loader!css-loader'
        }
      }
    ]
  },
  watch: true
};
