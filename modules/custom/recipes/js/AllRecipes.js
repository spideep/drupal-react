import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import 'whatwg-fetch'; // https://www.npmjs.com/package/whatwg-fetch

import Slider from "react-slick";

const AllRecipes = () => {
  const [list, setList] = useState([]);
  const [data, setData] = useState([[], []]);

  const endPoint = "/jsonapi/node/recipe?include=field_image";
  const fetchobj = {
    method: 'GET',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/vnd.api+json'
    }
  }

  let htmllist = [];

  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 2000,
    cssEase: "linear"
  }

  const getData = () => {
    fetch(endPoint, fetchobj)
      .then(response => response.json())
      .then(data => {
        const recipes = data.data.map(item => <li key={item.id}>{item.attributes.title}</li>);
        setList(recipes);
        setData([data.data, data.included]);
      });
  }

  let dlist = data[0].map((i, c) => {
    return <li key={i.id}>
      <h6>
        <a href={i.attributes.path.alias}>{i.attributes.title}</a>
      </h6>
      <img src={data[1][c].attributes.uri.url} />
      <p>
        {i.attributes.field_summary.value.substr(0, 100)}...
      </p>
    </li>
  });

  htmllist = list.length > 0 ? <Slider className="recipes_results" {...settings} id="recipes_results">{dlist}</Slider> : <ul><li>Nothing to show</li></ul>

  return (
    <div>
      <button onClick={getData}>Bring me other recipes</button>
      {htmllist}
    </div>
  )
}

export default AllRecipes;

ReactDOM.render(<AllRecipes />, document.querySelector('#recipes'));
